#+TITLE: ~(ice-9 filesystem)~
#+AUTHOR: Liliana Marie Prikler

~(ice-9 filesystem)~ provides a set of utilities to deal with the filesystem
beyond what POSIX mandates, from manipulating file names to actually creating
files and directories.

* Roadmap
** 0.2
- ~stat~ wrappers (~file-is-directory?~ etc.)
** 0.3
- More ~ftw~ wrappers? (perhaps some ~find-files~?)
** 1.0
- Rename to ~(ice-9 file-system)~ (as suggested in Guix mailing list).
- Inclusion to Guile itself.
