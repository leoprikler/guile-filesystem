;; Copyright (C) 2020 Liliana Marie Prikler <liliana.prikler@gmail.com>
;;
;; This library is free software; you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public
;; License as published by the Free Software Foundation; either
;; version 3 of the License, or (at your option) any later version.
;;
;; This library is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Lesser General Public License for more details.
;;
;; You should have received a copy of the GNU Lesser General Public
;; License along with this library; if not, write to the Free Software
;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

(use-modules (srfi srfi-64)
             (ice-9 filesystem))

(test-begin "destructive.scm")

(let* ((prefix "/tmp/destructive-")
       (dir (false-if-exception (mkdtemp! (string-append prefix "XXXXXX")))))
  (if dir
      (test-assert "tmpdir created" (file-is-directory? dir))
      (begin
        (test-expect-fail "tmpdir created")
        (test-assert "tmpdir created" dir)))
  (unless (test-passed?)
    (test-skip most-positive-fixnum))

  (test-assert "chdir tmpdir" (begin (chdir dir) #t))

  (test-assert "mkdir a"
    (let ((f (file-name-join* dir "a")))
      (mkdir f)
      (file-is-directory? f)))

  (test-assert "symlink b" (symlink "a" "b"))
  (test-equal "symlink can be directory"
    (list #f #t)
    (list (file-is-directory? "b")
          (file-is-directory? "b" stat)))

  (test-equal "canonicalize-file-name 1"
    (file-name-join* dir "a")
    (canonicalize-file-name "b"))

  (test-error "canonicalize-file-name 2"
    #t
    (canonicalize-file-name "does-not-exist"))

  (test-equal "canonicalize-file-name 3"
    (file-name-join* dir "a" "does-not-exist")
    (canonicalize-file-name "b/does-not-exist" #t))

  (test-equal "mkfifo"
    #t
    (begin
      (mkfifo "fifo")
      (file-is-fifo? "fifo")))

  (test-equal "fifo is other"
    #t
    (file-is-other? "fifo"))

  (test-assert "mkdir-p"
    (let ((f (file-name-join* "c" "d" "e")))
      (make-directories f)
      (file-is-directory? f)))

  (test-assert "delete-recursively"
    (begin
      (delete-file-recursively dir)
      (not (file-exists? dir)))))

(test-end "destructive.scm")
