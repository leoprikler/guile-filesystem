;; Copyright (C) 2020 Liliana Marie Prikler <liliana.prikler@gmail.com>
;;
;; This library is free software; you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public
;; License as published by the Free Software Foundation; either
;; version 3 of the License, or (at your option) any later version.
;;
;; This library is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Lesser General Public License for more details.
;;
;; You should have received a copy of the GNU Lesser General Public
;; License along with this library; if not, write to the Free Software
;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

(use-modules (srfi srfi-64)
             (ice-9 filesystem))

(test-begin "file-ancestors")

(test-equal "basename 0" "bar" (file-basename "foo/bar"))
(test-equal "basename 1" "foo" (file-basename "foo/bar" 1))
(test-equal "absolute basename 0" "foo" (file-basename "/foo/bar" 1))
(test-equal "absolute basename 1" "/" (file-basename "/foo/bar" 2))

(test-equal "dirname 0" "foo" (file-dirname "foo/bar"))
(test-equal "dirname 1" "." (file-dirname "foo/bar" 1))
(test-equal "absolute dirname 0" "/foo" (file-dirname "/foo/bar"))
(test-equal "absolute dirname 1" "/" (file-dirname "/foo/bar" 1))

(test-equal "parent depth 1" "foo" (file-parent "foo/bar"))
(test-equal "parent depth 0" "." (file-parent "foo"))
(test-equal "absolute parent depth 0" "/foo" (file-parent "/foo/bar"))
(test-equal "absolute parent depth 1" "/" (file-parent "/foo"))

(test-equal "child depth 0" "./foo" (file-child "." "foo"))
(test-equal "child depth 1" "foo/bar" (file-child "foo" "bar"))
(test-equal "absolute child depth 0" "/foo" (file-child "/" "foo"))
(test-equal "absolute child depth 1" "/foo/bar" (file-child "/foo" "bar"))

(test-equal "absolute file-ancestors 0" '("/") (file-ancestors "/"))
(test-equal "absolute file-ancestors 2"
  '("/" "/foo" "/foo/bar") (file-ancestors "/foo/bar"))
(test-equal "relative file-ancestors 0"
  '("foo" "foo/bar") (file-ancestors "foo/bar"))

(test-end "file-ancestors")
