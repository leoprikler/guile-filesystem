(use-modules (guix gexp)
             (guix packages)
             (guix git-download)
             (guix utils)
             (guix build-system gnu)
             (gnu packages autotools)
             (gnu packages guile)
             (gnu packages pkg-config)
             (gnu packages texinfo)
             ((guix licenses) #:select (lgpl3+)))

(define %source-dir (dirname (current-filename)))

(define (guile-filesystem prefix guile)
  (package
    (name (string-append prefix "-filesystem"))
    (version "0.2.0")
    (source (local-file %source-dir
                        #:recursive? #t
                        #:select? (git-predicate %source-dir)))
    (build-system gnu-build-system)
    (native-inputs
     `(("autoconf" ,autoconf-wrapper)
       ("automake" ,automake)
       ("pkg-config" ,pkg-config)
       ("texinfo" ,texinfo)))
    (inputs
     `(("guile" ,guile)))
    (home-page "https://gitlab.com/leoprikler/guile-filesystem")
    (synopsis "Complementary library to Guile's built-in file system procedures")
    (description "@code{guile-filesystem} provides a set of utility functions,
that augment Guile's support for handling files and their names.")
    (license lgpl3+)))

(list
 (guile-filesystem "guile2.0" guile-2.0)
 (guile-filesystem "guile2.2" guile-2.2)
 (guile-filesystem "guile" guile-3.0)
 (guile-filesystem "guile-latest" guile-3.0-latest))
